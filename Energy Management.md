# Energy Management
## Manage Energy not Time

## Q1. What are the activities you do that make you relax - Calm quadrant?

- Some activities that make me relax are -
    1. Playing with my friends.
    2. Listening to music.
    3. Watching movies.

## Q2. When do you find getting into the Stress quadrant?

- We do the most urgent things first, not considering if it’s the most important. You may say yes, it’s normal because that is how things are usually done.

## Q3. How do you understand if you are in the Excitement quadrant?

- If you are someone who is not that emotionally intelligent, the good news is that it can be made better, and you can improve your level of emotional intelligence.

# Sleep is your superpower

## Q4. Paraphrase the Sleep is your Superpower video in detail.

- If we pick regular sleeping patterns, going to bed and waking at the same time – plus keeping cool when we wish to go to sleep will help us get into routines that enable us to sleep more effectively.

## Q5. What are some ideas that you can implement to sleep better?

- Relaxing your body, fixing a time to sleep, room temperature, exercising, and following a routine for having supper can help with better sleep.

# Brain-Changing Benefits of Exercise

## Q6. Paraphrase the video - Brain Changing Benefits of Exercise.

- There are many brain-Changing Benefits of Exercise. Some of them are-    
    - Maintaining focus longer than earlier.    
    - Helps reduce obesity and makes you feel good.    
    - Helps in tackling stress.
      


## Q7. What are some steps you can take to exercise more?

- You may notice that you need to increase the amount of time you exercise in order to continue improving. Or you may be pleasantly surprised to find that you're exercising just the right amount to meet your fitness goals.
