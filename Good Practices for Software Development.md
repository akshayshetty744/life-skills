# Review Questions

## Question 1

* Make sure to ask questions and seek clarity in the meeting itself. Since most of the times, it will be difficult to get the same set of people online again

* Implementation taking longer than usual due to some unexpected issue - Inform relevant team members

* Use Github gists to share code snippets. Use sandbox environments like Codepen, Codesandbox to share the entire setup

* Join the meetings 5-10 mins early to get some time with your team members

* Be available when someone replies to your message. Things move faster if it's a real-time conversation as compared to a back and forth every hour.

* Always keep your phone on silent. Remove notifications from the home screen. Only keep notifications for work-related apps.


## Question 2

### Which area do you think you need to improve on? 
* I am stuck in logical part I need improve on that when i trying build logics it will take too much time 
  like get data from array of object that kind of things i am stuck definitely i will improve on that soon.

### What are your ideas to make progress in that area?
* On Hacker Rank i will solve questions daily to improve my logical part without using any google it.
