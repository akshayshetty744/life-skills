# Learning Process

## 1. How to Learn Faster with the Feynman Technique

### What is the Feynman Technique?

* If you want to understand something well, try to explain it simply.

* The point here is that I think this is one part of what teaching me how to think is supposed to mean. To be just a little less arrogant.
  
### What are the different ways to implement this technique in your learning process?

1) You must not fool yourself  if you do you are the first person to fool

2) Note down the topics which you want to do take a paper and write at top of the note in simple words.

3) Identify the problem areas then go through solve it 

4) Do complicated things and make it a simple way to do yourself

5) When you know the concept just check how you will be simply explaining it to others.

## 2. Learning How to Learn TED talk by Barbara Oakley

### Paraphrase the video in detail in your own words.

* Describes Focused Thinking as dependent on established neural ways It’s the type of thinking you access to understand and solve familiar problems as well as to study a concept.

* Diffuse Thinking is what happens when your mind relaxes, Diffuse Thinking allows your subconscious to make unexpected connections between disparate ideas.

### What are some of the steps that you can take to improve your learning process?
1) In focused mode we are thinking only about what we are going to do in this a won't affect our learning.

2) When it comes to the diffused mode it will different we feel unstable so in this case, we just take a break.

3) Let's try to think differently or take a break for different places to feel better than we feel better to try to make the point where we are going to be stuck in learning.

4) Our mind is try to go into diffused mode frequently just take a break and relax your mind after that you feel better.

## 3. Learn Anything in 20 hours

### Your key takeaways from the video? Paraphrase your understanding.

* When we are learning something just focus on one point and try to get understand only one topic at a time.

* Don't waste your time learning one thing if you wasting time on one topic suppose you wasting time let's say 1000 hours on one thing it won't work for anyone.

* For learning concepts it will 20 hours is enough just to be focused

### What are some of the steps that you can while approaching a new topic.

1) Note down where you are going to stuck start when learning on particular topics.

2) Make a timetable for points on one topic that we are going to learn.

3) Take a break for some time and keep your mind in as a fresh and set time to complete a task.  

