# Tiny Habits - BJ Fogg

### Q1. Your takeaways from the video.
- There are 2 ways of long-term behavior you need to change your behavior and then you need to change people around you. This inspires me to change.

- Two things must and should have for long-term behavior are motivation and the ability to I will make myself get motivated and complete the task.

# Tiny Habits by BJ Fogg - Core Message

### Q2. Your takeaways from the video in as much detail as possible.

- one needs to change his behavior there are three prompts one can change his daily routine there External Prompts, Internal prompts, and External prompts most people failed to do it but we need to go step by step. Little steps taken consistently get further and closest to the once-desired goal. celebration is important because the more you celebrate the more you get motivated and make it a habit.


### Q3. How can you use B = MAP to make making new habits easier?

- Behavior is the result of motivation, ability, and prompts taken at the same time. This is a simple formula that can lead to incredible results. In other words, a behavior is the result of Motivation, or your desire to execute the behavior.Ability, i.e. your capacity to execute the behavior.Prompt, or your cue to execute the behavior.This is true for building a habit and breaking an old habit in equal measure.


### Q4. Why it is important to "Shine" or Celebrate after each successful completion of a habit?

- Feeling good is a vital part of the Tiny Habits method. You can create this good feeling by using a technique I call “celebration.” When you celebrate, you create a positive feeling inside yourself on demand. This good feeling wires the new habit into your brain.

# 1% Better Every Day Video

### Q5. Your takeaways from the video.

- Getting tiny tasks successful one need to work on the start and make it easy and simple. A little improvement every day lead to achieving bigger goals in the long term process.Kill the habit: Think about in half a years time if you didn't fulfill the habits, why? Then plan accordingly to avoid 

# Book Summary of Atomic Habits

### Q6. Write about the book's perspective on habit formation from the lens of Identity, processes, outcomes?

- The book suggested many ways of adapting any habit and making that habit your style of living,With outcome-based habits, the focus is on what you want to achieve. These types of habits are the weekly, daily, minute-by-minute habits breaking bad behaviors and adopting good ones in four steps, showing you how small, incremental, everyday routines compound into massive, positive change over time

### Q7. Write about the book's perspective on how to make a good habit easier?

Building good habits in 5 steps When building habits, you will often be replacing old ones. Here are seven ways to replace bad habits with healthier ones:

- Do meditation 
- Make a negative habit difficult
- wake up early morning 
- Swap a bad habit for a better habit
- Build motivation

### Q8. Write about the book's perspective on making a bad habit more difficult?

- Good habits are so important because they give us those 1% productivity gains on a daily basis, making them extra powerful in terms of compound interest.

# Reflection:

### Q9. Pick one habit that you would like to do more of? What are the steps that you can take to make it make the cue obvious or the habit more attractive or easy and or response satisfying?

- A handful of problems arise when you spend too much time thinking about your goals and not enough time designing you systems: winners and losers have the same goal; achieving a goal is only a momentary change; goals restrict your happiness; goals are at odds with long term progress.

### Q10. Pick one habit that you would like to eliminate or do less of? What are the steps that you can take to make it make the cue invisible or the process unattractive or hard or the response unsatisfying?

- Watching TV is my bad habit. I want to change this habit of mine and change I will do wake up early day by day I will make it like a habit.
