 
# Focus Management
## What is Deep Work

### Q1. What is Deep Work?

- Deep work is defined as a “professional activity performed in a state of distraction-free concentration that pushes your cognitive capabilities to their limit.
- This type of work is cognitively demanding and therefore requires longer stretches of sustained focus to complete.
- Deep work increases productivity, creative thinking, and positive feelings of engagement with our work. When we successfully dive into focused work.

## Summary of Deep Work Book

### Q2. Paraphrase all the ideas in the above videos and explain one in detail.

- There are following ideas-
    1. Schedule your distraction and train your brain to take the distraction breaks in preplanned ways.
    2.  Deep work ritual- generate rhythm of work to do deep work.
    3.  Daily shutdown complete ritual -sleep is the price we pay for the intense deep work.

### Q3. How can you implement the principles in your day to day life?

 1. Develop habits and build a routine
 - Good habits set us up for long-term success and make us feel good
 - Good habits would help you reach your goals. 

 2. Planning priorities ahead
 - Life has unpredictable twists and turns. That doesn’t mean a little planning doesn’t go a long way.
 - Overestimate how long a task will take you
 - Plan and schedule in the small things. 
## Dangers of Social Media

### Q4. Your key takeaways from the video.

- In order to do your work with full attention it is important to hold yourself from fragmenting your attention. If you are fragmenting your attention then it will not take long to result in the incapability of not being able to concentrate on your task for long. Social media is harmless and it is not an essential part of life, actually, life is beyond social media.
